import numpy as np

class Model():
  #this sia  simple probabilistic model representing our sensor
  def __init__ (self):
    self.StartPoint = 10
    self.EndPoint   = 50
    self.noise      = 10
    self.NumPoints  = 100

  def ProcessCreation(self,NumPoints=100):
    out=np.linspace(self.StartPoint, self.EndPoint,num=NumPoints )
    out+=np.random.normal(loc=0.0, scale=self.noise, size=NumPoints) 
    return out
