import numpy as np

class Model():
  #this sia  simple probabilistic model representing our sensor
  def __init__ (self):
    self.State = np.random.randint(3)
    self.ObservationMatrix = [[0.5,0.2 ,0.3],[0.3 ,0.6, 0.1],[0.1, 0.1, 0.8]]
    
  def GenerateState(self):
    #this function generates a random hidden state
    self.State = np.random.randint(3)
    
    
  def GetState(self):
    #this is a function we can not use inn practice, it reveals the hidden state. We will use this to check our result.
    return self.State
      
  
  def Measure(self):
    state = np.zeros((3))
    state[self.State] = 1
    vect = np.matmul(state,self.ObservationMatrix)
    r = np.random.rand()
    if vect[0] > r:
      return 0
    elif r > vect[0] and r < (vect[0]+vect[1]):
      return 1
    else:
      return 2
    
  def MeasureKnownState(self,X):
    state = np.zeros((3))
    state[X] = 1
    vect = np.matmul(state,self.ObservationMatrix)
    r = np.random.rand()
    if vect[0] > r:
      return 0
    elif r > vect[0] and r < (vect[0]+vect[1]):
      return 1
    else:
      return 2
